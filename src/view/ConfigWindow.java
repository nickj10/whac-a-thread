package view;

import controller.ConfigButtonController;

import javax.swing.*;
import java.awt.*;

public class ConfigWindow extends JFrame {
    private JTextField jtfNumberMoles;
    private JTextField jtfBoardSize;
    private JLabel jlNumberMoles;
    private JLabel jlBoardSize;
    private JButton jbSave;

    public ConfigWindow(){
        // Creem la vista

        // Zona superior
        JPanel jpSuperior = new JPanel();
        JPanel aux1 = new JPanel();
        aux1.setLayout(new GridLayout(2,1));
        jlNumberMoles = new JLabel("Number of moles:");
        jtfNumberMoles = new JTextField("");
        jtfNumberMoles.setPreferredSize(new Dimension(50, 25));
        jtfNumberMoles.setEditable(true);
        aux1.add(jlNumberMoles);
        aux1.add(jtfNumberMoles);
        jpSuperior.add(aux1);

        getContentPane().add(jpSuperior, BorderLayout.PAGE_START);

        JPanel jpCentral = new JPanel();
        JPanel aux2 = new JPanel();
        aux2.setLayout(new GridLayout(2,1));
        jlBoardSize = new JLabel("Board game size:");
        jtfBoardSize = new JTextField("");
        jtfBoardSize.setPreferredSize(new Dimension(50, 25));
        jtfBoardSize.setEditable(true);
        aux2.add(jlBoardSize);
        aux2.add(jtfBoardSize);
        jpCentral.add(aux2);

        getContentPane().add(jpCentral, BorderLayout.CENTER);


        // Zona Inferior
        JPanel jpInferior = new JPanel();
        jpInferior.setLayout(new FlowLayout());
        jbSave = new JButton("Save");
        jpInferior.add(jbSave);

        getContentPane().add(jpInferior, BorderLayout.PAGE_END);


        // Donem una mida a la finestra
        setSize(250, 200);
        setResizable(false);

        // Li posem un títol
        setTitle("Config");

        // Indiquem que s'aturi el programa si fab clic a la "X" de la finestra
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // Indiquem que la finestra se situï al centre de la pantalla
        setLocationRelativeTo(null);
    }

    public void showConfigDetailsError(){
        JOptionPane.showMessageDialog(this,
                "The number of moles cannot be greater than the total number of game board boxes",
                "Warning",
                JOptionPane.WARNING_MESSAGE);
    }

    public int getNumberMoles() {
        return Integer.parseInt(jtfNumberMoles.getText());
    }

    public int getBoardGameSize() {
        return Integer.parseInt(jtfBoardSize.getText());
    }

    public void assignButtonController(ConfigButtonController bController){
        // Registra el botó Save
        jbSave.addActionListener(bController);
    }
}
