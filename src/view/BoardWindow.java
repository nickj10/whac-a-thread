package view;

import controller.TalpController;

import javax.swing.*;
import java.awt.*;

public class BoardWindow extends JFrame{
    private TalpPanel[][] jpTalps;

    public BoardWindow(int boardSize) {
        getContentPane().setLayout(new GridLayout(boardSize, boardSize));
        jpTalps = new TalpPanel[boardSize][boardSize];
        for (int i = 0; i < jpTalps.length; i++) {
            for (int j = 0; j < jpTalps[i].length; j++) {
                jpTalps[i][j] = new TalpPanel(i, j);

                getContentPane().add(jpTalps[i][j]);
            }
        }
        int panelSize = boardSize * 100;
        setSize(panelSize, panelSize);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Moles Game");
    }

    public void assignTalpController(TalpController tController){
        // Registra les caselles del board
        for(int i = 0; i < jpTalps.length; i++) {
            for(int j = 0; j < jpTalps[i].length; j++) {
                jpTalps[i][j].addMouseListener(tController);
            }
        }
    }

    public void showTalp(int row, int column) {
        jpTalps[row][column].setBackground(new Color(102,51,0));
    }

    public void hideTalp(int row, int column) {
        jpTalps[row][column].setBackground(Color.LIGHT_GRAY);
    }

    public boolean talpIsVisible(int posX, int posY) {
        Color brown = new Color(102,51,0);
        return jpTalps[posX][posY].getBackground().equals(brown);
    }
}
