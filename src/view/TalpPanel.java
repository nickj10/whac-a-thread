package view;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class TalpPanel extends JPanel {
    private int posX;
    private int posY;

    public TalpPanel(int posX, int posY) {
        setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        setBackground(Color.LIGHT_GRAY);
        this.posX = posX;
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

}
