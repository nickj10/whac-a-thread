package controller;

import model.GameManager;
import model.Observer;
import model.Position;
import view.BoardWindow;
import view.TalpPanel;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class TalpController implements MouseListener, Observer {
    private BoardWindow vista;
    private GameManager model;

    public TalpController(BoardWindow vista, GameManager model) {
        this.vista = vista;
        this.model = model;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        TalpPanel talp = (TalpPanel)e.getSource();
        if (model.getBoard().containsTalp(new Position(talp.getPosX(), talp.getPosY()))) {
            if (vista.talpIsVisible(talp.getPosX(), talp.getPosY())) {
                vista.hideTalp(talp.getPosX(), talp.getPosY());
                model.incrementScore();
                model.showScore();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void onOccupyPosition(int id, Position position) {
        //System.out.println("topo id: " + id + " en la pos [" + position.getX() + "," + position.getY() + "]");
        vista.showTalp(position.getX(), position.getY());
    }

    @Override
    public void onFreePosition(int id, Position position) {
        //System.out.println("topo id: " + id + " liberada de la pos [" + position.getX() + "," + position.getY() + "]");
        vista.hideTalp(position.getX(), position.getY());
    }
}
