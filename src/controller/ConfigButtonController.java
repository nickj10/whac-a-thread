package controller;

import model.GameBoard;
import model.GameManager;
import view.BoardWindow;
import view.ConfigWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConfigButtonController implements ActionListener {
    private ConfigWindow vista;
    private GameManager model;

    public ConfigButtonController(ConfigWindow vista, GameManager model) {
        this.model = model;
        this.vista = vista;
    }

    public void actionPerformed(ActionEvent e) {
        // Recuperem el text del botó
        String boto = ((JButton)e.getSource()).getText();
        if (boto.equals("Save")) {
            if (vista.getNumberMoles() > (vista.getBoardGameSize() * vista.getBoardGameSize())) {
                vista.showConfigDetailsError();
            } else {
                // Guardem les dades del joc
                model.setBoard(new GameBoard(vista.getNumberMoles(), vista.getBoardGameSize()));

                // Creem la nova vista del board amb les dades
                BoardWindow board = new BoardWindow(model.getBoard().getBoardSize());
                TalpController talpController = new TalpController(board, model);
                board.assignTalpController(talpController);

                // Assignem l'observador al model per als talps
                model.registerObserver(talpController);
                model.createTalps();

                // Fem la nova vista visible
                board.setVisible(true);
            }
        }
    }

}
