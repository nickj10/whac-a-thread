package model;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;

import static java.lang.Thread.sleep;

public class Talp implements Runnable {
    private final int[][] gameBoard;
    private final int id;
    private static final int WAIT_TIME = 3000;
    private Lock lock;
    private Observer observer;


    public Talp(int[][] gameBoard, int id, Lock lock, Observer observer) {
        this.id = id;
        this.lock = lock;
        this.gameBoard = gameBoard;
        this.observer = observer;
    }


    public void run() {
        try {

            while(true) {
                int randomSegonsApareix = ThreadLocalRandom.current().nextInt(1, 6);
                sleep(randomSegonsApareix * 1000);

                while (true) {
                    int randomRowPosition = ThreadLocalRandom.current().nextInt(0, gameBoard[0].length);
                    int randomColPosition = ThreadLocalRandom.current().nextInt(0, gameBoard[0].length);
                    //lock.lock();
                    if (gameBoard[randomRowPosition][randomColPosition] == 0) {
                        // Assignar el talp a una posició
                        gameBoard[randomRowPosition][randomColPosition] = id;
                        //lock.unlock();

                        // Deixa que el talp occupi la posició i esperar 3 segons
                        observer.onOccupyPosition(id, new Position(randomRowPosition, randomColPosition));
                        sleep(WAIT_TIME);

                        // Deixa anar el talp perquè vagi buscant una altra posició
                        gameBoard[randomRowPosition][randomColPosition] = 0;
                        observer.onFreePosition(id, new Position(randomRowPosition, randomColPosition));

                        break;
                    }
//                    else {
//                        System.out.println("topo id: " + id + " pos [" + randomRowPosition + "," + randomColPosition + "] ocupada");
//                    }
//                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Getters i Setters
    public int getId() {
        return id;
    }

    public int[][] getGameBoard() {
        return gameBoard;
    }

    public static int getWaitTime() {
        return WAIT_TIME;
    }

    public Lock getLock() {
        return lock;
    }

    public void setLock(Lock lock) {
        this.lock = lock;
    }

    public Observer getObserver() {
        return observer;
    }

    public void setObserver(Observer observer) {
        this.observer = observer;
    }
}
