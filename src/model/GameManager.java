package model;

import java.util.concurrent.locks.ReentrantLock;

public class GameManager {
    private GameBoard board;
    private Talp[] talps;
    private int score;
    private ReentrantLock lock;
    // Simplificació de patró Observador amb un únic observer
    private Observer observer;

    public GameManager(GameBoard board) {
        score = 0;
        lock = new ReentrantLock();
        this.board = board;
    }

    public GameManager() {
        score = 0;
        lock = new ReentrantLock();
    }

    // Mètodes
    public void showScore() {
        System.out.println("Total score: " + score);
    }

    public void incrementScore() {
        score++;
    }

    public void createTalps() {
        talps = new Talp[board.getNumMoles()];
        for (int i = 0; i < talps.length; i++) {
            talps[i] = new Talp(board.getGameBoard(), i + 1, lock, this.observer);
            Thread t = new Thread(talps[i]);
            t.start();
        }
    }

    public void registerObserver(Observer o) {
        this.observer = o;
    }

    // Getters i Setters
    public GameBoard getBoard() {
        return board;
    }

    public void setBoard(GameBoard board) {
        this.board = board;
    }

    public Talp[] getTalps() {
        return talps;
    }

    public void setTalps(Talp[] talps) {
        this.talps = talps;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
