package model;

public class GameBoard {
    private int numMoles;
    private int boardSize;
    private int[][] gameBoard;

    public GameBoard(int numMoles, int boardSize) {
        this.numMoles = numMoles;
        this.boardSize = boardSize;
        gameBoard = new int[boardSize][boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                gameBoard[i][j] = 0;
            }
        }
    }

    public boolean containsTalp(Position pos) {
        return gameBoard[pos.getX()][pos.getY()] > 0;
    }

    public int getNumMoles() {
        return numMoles;
    }

    public void setNumMoles(int numMoles) { this.numMoles = numMoles; }

    public int getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(int boardSize) {
        this.boardSize = boardSize;
    }

    public int calculaNumTotalCaselles() {
        return boardSize * boardSize;
    }

    public int[][] getGameBoard() {
        return gameBoard;
    }

    public void setGameBoard(int[][] gameBoard) {
        this.gameBoard = gameBoard;
    }
}
