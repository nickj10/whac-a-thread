package model;

public interface Observer {
    void onOccupyPosition(int id, Position position);
    void onFreePosition(int id, Position position);
}
