import controller.ConfigButtonController;
import model.GameManager;
import view.ConfigWindow;

import javax.swing.*;

public class WhacAThread {
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Creem la vista
                ConfigWindow configView = new ConfigWindow();

                // Creem el model
                GameManager model = new GameManager();
                // Creem el controlador de botons
                ConfigButtonController configButtonControl = new ConfigButtonController(configView, model);

                // Registrem els botons
                configView.assignButtonController(configButtonControl);


                //Fem la vista visible
                configView.setVisible(true);
            }
        });
    }
}
